# vue_project

## 知识点
### Promise
  Promise 对象有两个属性：promiseState 和 promiseResult
  promiseState 有三种状态：pending, fulfilled, rejected
### 冒泡
  当一个元素上的事件被触发时，具有同样事件的祖先元素都会被触发，每个祖先元素都会被触发，看不到效果是因为可能没有给祖先元素绑定事件处理程序。
  条件：
    1. 元素嵌套
    2. 具有相同事件名

#### 事件源
  - 当前事件源：当前正在触发事件，正在执行事件处理程序的元素
  - 事件三要素：事件源 - 事件类型（名称） - 事件处理程序（回调函数）

#### 事件对象
  - event.target：冒泡的起始位置，最开始的事件源
  - this：this 永远指向执行事件处理程序的元素对象

### 标签的自定义属性
  - 元素自定义属性直接通过 打点 的方式是获取不到的，需要通过 getAttribute 方法获取。
  - 元素自带的属性，可以直接 打点 获取，也可以使用 getAttribute 方法获取。
  - 一般使用 data- 开头，最终会被放入 dataset 属性中，dataset 是一个对象。

### 转换成 Boolean 类型为 false 的6中情况
  - 0 NaN '' undefined null false

### 单文件组件中 name 的作用：3种
  - vue 插件（dev-tools）中显示的名称，方便观察
  - 使用 keep-alive 组件缓存路由组件时，用在 include 属性中
  - 全局注册组件时，组件.name 可以获取到组件的名称

### VueRouter配置中 params 参数中的 ？号
  - 代表此参数为 undefined 或 null 时，自动忽略。但是不能为 '' 空字符串

### 通过路由给组件传参
  - 配置路由的 props 参数，能够给路由传递 props 参数，降低组件和路由之间的耦合，提高复用率
  - 三种形式：
    - Boolean 值：表示此路由的 params 参数映射为组件的 props
    - 对象：对象中的值映射为组件的 props
    - 函数：参数是 当前路由规则对象，返回值是一个对象，对象的值映射为组件的 props

### 模拟服务器
  - mockjs：
  - json-server：模拟服务器

### 前后端数据交互的格式只有两种
  - 字符串：纯文本 或 json 字符串
  - 二进制：文件，图片，音视频

### 监视路由规则
  - 每次路由变化都会产生一个新的路由规则对象。
  - 所以：只需要监听 $route，而不需要深度监听

### 分页
  - 当一个页面中的数据过多或多到加载过慢的时候，可以使用分页
  - total：数据总数
  - pageNo：当前页码
  - pageSize：每页的数量
  - continues：连续页码
  - totalPage：总的页码数
  - 连续页码的实现算法：连续范围：end - start + 1

### vue2 watch 侦听数组变化
  - 如果要监听数组中对象的属性数据的变化，则需要开启深度监视

### 放大镜的实现方法
  - 在主图和 mask 遮罩层的同级新建一个div，并为绝对定位，放到最顶层（默认背景为透明，效果上不会遮盖其他元素），根据鼠标在此div 上的位置（offsetX/Y）去计算 mask 的位置

### localStorage 和 sessionStorage
  - localStorage：永久性存储，除非手动或编码删除。
  - sessionStorage：暂时性存储，浏览器会会话结束后就会清除。
  - 两者 Api 一致，getItem，setItem，removeItem

### token 和 uuid/nanoid
  - token：在后端（服务器）生成，返回给前端
  - uuid/nanoid：前端生成的

### boolean 的隐式转换
  - 使用 !!x
  - 例：let x = 5,  let bol = !!x, // true

### keyup、keydown、change、blur 事件的触发时机以及


### dispatch 方法的返回值
  - dispatch 方法返回的是一个 Promise 对象，若在 actions 中发送登录请求，这个action 可以认为是异步的，要等到请求响应的结果返回后才可以去跳转，不然可能会登录失败后依然跳转了。

### vuex 中的数据持久化存储
  - 初始化时，在 vuex 中读取 localStorage 中的数据
  - 登录成功时，把用户信息更新到 vuex 和 localStorage 中

### 优化1：路由懒加载
  - 使用动态加载模式，import() 函数，动态加载路由，
  - 把不同路由对应的代码分割成不同的代码块，当路由被访问时才加载对应代码
  - 能够有效减少首次加载代码体积能提高首屏加载速度，是一种优化方式。

### 优化2：关闭 productionSourceMap
  - productionSourceMap 能够帮助我们定位错位的位置，但是对于上线后，这个功能的意义是不大的，
  - 且打包后的 .map 文件是很大的，通常是好几M，所以可以关闭 productionSourceMap。

## 流程
 1. vue 电商前台项目

 2. 利用 vue-cli 初始化项目
    - 构建项目基本结构
    - vue create my-app
 
 3. 组件的划分
    搭建基本框架：创建 Header、Footer 公共组件及其他组件
 
 4. 配置路由
 
 5. 二次封装 axios
    - 统一设置请求的基础路径，方便以后的维护
    - 添加请求拦截器：发送请求会先执行请求拦截器，可以做：统一设置进度条、在请求头设置 token
    - 添加响应拦截器：请求被响应回来后先执行响应拦截器，可以做：统一提取返回数据的 data
 
 6. 发送请求获取三级导航
    - 统一设置请求接口函数

 7. 使用 Vuex 来存储数据
    - 多个组件共用的数据，存储到 Vuex

 8. 配置代理
    - 开发时配置代理服务器，解决开发时的 跨域问题
 