const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  productionSourceMap: false,
  lintOnSave:false,
  // devServer: {
  //   // proxy:{
  //   //   "/api": {
        
  //   //   }
  //   // }
  //   // open: true,
  // }
  devServer: {
    proxy: {
      "/api": {
        // http://gmall-h5-api.atguigu.cn
        target: 'http://gmall-h5-api.atguigu.cn',
        changeOrigin: true
      }
    }
  },
  // productionSourceMap:false,
  publicPath: process.env.NODE_ENV === 'production'
  ? '/vue/shop/'
  : '/'
})
