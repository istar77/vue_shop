import {v4 as uuidv4} from 'uuid'

export default () => {
  let userTempId = localStorage.getItem('userTempId')

  // 如果本地不存在uuid，则生成一个存入本地
  !userTempId && localStorage.setItem('userTempId', uuidv4())

  return userTempId
}