// 导入验证规则
import {extend} from 'vee-validate'
import {required, } from 'vee-validate/dist/rules'
import {codeNumReg, phoneNumReg, passwordReg} from '@/utils/reg'

extend('required', {
  ...required,
  message: '此项为必填项！'
})
extend('phone', {
  validate(value){
    return phoneNumReg.test(value) || '手机号格式错误！'
  },
})

extend('code', {
  validate(value){
    return codeNumReg.test(value) || '验证码错误，请重新输入！'
  }
})

extend('password', {
  validate(value){
    return passwordReg.test(value) || '密码错误，请重新输入！'
  }
})

extend('passwordAgain', {
  validate(value, {password}){
    return passwordReg.test(value) || '两次密码不一致，请重新输入！'
  },
  params: ['password']
})

extend('isAgree', {
  validate(value){
    return value
  },
  message: '请勾选协议...'
})
