import axios from 'axios'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import getUserTempId from './getUserTempId'

const url = 'http://gmall-h5-api.atguigu.cn'

const http = axios.create({
  baseURL: 'http://gmall-h5-api.atguigu.cn/api', // 打包上线时的 api
  // baseURL: '/api',
  timeout: 10000,
  headers: {
  }
}, error => {
  return Promise.reject(error.message)
})

// 请求拦截器
http.interceptors.request.use((config) => {
  // 配置请求头 
  config.headers.userTempId = getUserTempId()
  config.headers['token'] = localStorage.getItem('token')
  
  NProgress.start() // 开启进度条
  return config
})

// 响应拦截器
http.interceptors.response.use((res)=>{
  NProgress.done() // 关闭进度条

  return res.data
}, error => {
  // 输出错误原因
  console.log(error.message)

  NProgress.done() // 关闭进度条
  return Promise.reject(error.message) // 返回失败的 Promise 对象
})


export default http

