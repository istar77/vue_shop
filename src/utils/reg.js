/**
 * 此文件专门用来定义正则的文件
 */
export const skuNumReg = /^[1-9]\d{0,2}$/

export const phoneNumReg = /^1(3[0-9]|5[0-35-9]|6[67]|7[0-3]|8[0-9])\d{8}$/

export const codeNumReg = /^\d{6}$/

export const passwordReg = /(?!^(\d+|[a-zA-Z]+|[~!@#$%^&*()_.]+)$)^[\w~!@#$%^&*()_.]{8,16}$/

