import axios from 'axios'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

const mockHttp = axios.create({
  baseURL: '/mock',
  timeout: 6000,
})

mockHttp.interceptors.request.use(config => {
  NProgress.start()
  return config
}, error => Promise.reject(error.message))

mockHttp.interceptors.response.use(res=> {
  NProgress.done()

  return res.data
}, error => {
  NProgress.done()
  return Promise.reject(error.message)
})

export default mockHttp