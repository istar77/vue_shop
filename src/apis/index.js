import http from "@/utils/http"
import mockHttp from "@/utils/mockHttp"

// 三级分类
export const reqGetCategoryList = () => {
  return http.get('/product/getBaseCategoryList')
}

// 获取搜索商品的信息
export const reqGetGoodsList = (params = {}) => {
  return http.post('/list', params)
}

export const reqGetBannerList = () => {
  return mockHttp.get('/banner')
}

export const reqGetFloorsList = () => {
  return mockHttp.get('/floors')
}

// 获取单个商品的详细信息
export const reqGetItemDetail = (skuId) => {
  return http.get(`/item/${skuId}`)
}

// 6. 添加到购物车(对已有物品进行数量改动)
export const reqAddToCard = (skuId, skuNum) => {
  return http.post(`/cart/addToCart/${skuId}/${skuNum}`)
}

// 获取购物车列表
export const reqGetCartList = () => {
  return http.get('/cart/cartList')
}

// 切换商品选中状态
export const reqGetUpdateCheckCart = (skuID, isChecked) => {
  return http.get(`/cart/checkCart/${skuID}/${isChecked}`)
}

// 删除购物车商品
export const reqDeleteCart = skuId => {
  return http.delete(`/cart/deleteCart/${skuId}`)
}


// 注册
export const reqUserRegister = (regParams) => {
  return http.post(`/user/passport/register`, regParams)
}

// 登录
export const reqUserLogin = (logParams) => {
  return http.post(`/user/passport/login`, logParams)
}

// 发送验证码
export const reqUserSendCode = phone => {
  return http.get(`/user/passport/sendCode/${phone}`)
}

// 退出登录
export const reqUserLogout = () => {
  return http.get(`/user/passport/logout`)
}

// 获取用户地址
export const reqGetUserAddress = () => {
  return http.get(`/user/userAddress/auth/findUserAddressList`)
}

// 获取订单交易页信息
export const reqGetTrade = ()=> {
  return http.get(`/order/auth/trade`)
}

// 提交订单
export const reqSubmitOrder = (goods) => {
  return http.post(`/order/auth/submitOrder?tradeNo=${goods.tradeNo}`, goods)
}

// 获取订单支付信息
export const reqGetPay = orderId => http.get(`/payment/weixin/createNative/${orderId}`)

// 查询支付订单状态
export const reqQueryPayStatus = orderId => http.get(`/payment/weixin/queryPayStatus/${orderId}`)

// 获取我的订单列表
export const reqGetOrder = (page = 1, limit = 5) => http.get(`/order/auth/${page}/${limit}`)

// // 根据 token 获取用户登录信息
// export const reqGetUserInfo = 