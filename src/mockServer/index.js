import Mock from 'mockjs'

// 引入 .json 文件，会自动转换为真正的数组或对象
import banner from './banner.json'
import floors from './floors.json'

Mock.mock('/mock/banner', {
  code: 200,
  msg: 'ok',
  data: banner
})

Mock.mock('/mock/floors', {
  code: 200, 
  msg: 'ok',
  data: floors
})

