import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import home from './modules/home'
import search from './modules/search'
import user from './modules/user'
import trade from './modules/trade'

export default new Vuex.Store({
  modules: {
    home,
    search,
    user,
    trade,
    Todos: {
      state: {a:1}
    }
  }
})