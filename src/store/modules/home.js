/**
 * 当前页面时 home 模块，也就是主页面中的数据
 */

import { reqGetCategoryList } from "@/apis" 

const state = {
  categoryList: [], 
}
const actions = {
  async getCategoryList(context){
    const res = await reqGetCategoryList()
    // 成功 / 失败
    res.code === 200 ? context.commit('getCategoryList', res.data.slice(0, 15)) : console.log(res.message)
  }
}
const mutations = {
  getCategoryList(state, categoryList) {
    state.categoryList = categoryList
  }
}
const getters = {

}

export default {
  namespaced: true, 
  state,
  actions,
  mutations,
  getters
}