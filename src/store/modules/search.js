import {reqGetGoodsList} from '@/apis/index'


export default {
  namespaced: true, // 启用命名空间，区分不同的模块
  state: {
    goodsInfo: {},
  },
  actions: {
    async reqGetGoodsList({commit}, params){
      const res = await reqGetGoodsList(params)
      
      res.code === 200 ? commit('reqGetGoodsList', res.data) : 
      alert(res.message)
      
    }
  },
  mutations: {
    reqGetGoodsList(state, goodsInfo){
      state.goodsInfo = goodsInfo
    }
  },
  getters: {
    trademarkList(state, getters) {
      return state.goodsInfo?.trademarkList
    },
    attrsList(state, getters) {
      return state.goodsInfo?.attrsList
    },
    goodsList(state, getters) {
      return state.goodsInfo?.goodsList
    },
    total(state){
      return state.goodsInfo?.total
    },
    totalPages(state) {
      return state.goodsInfo?.totalPages
    }
  }
}