import { reqGetTrade } from "@/apis"
const state = {
  tradeInfo: {}
}

const actions = {
  async getTrade({commit}, payload) {
    const res = await reqGetTrade()
    if(res.code == 200) {
      commit('setTrade', res.data)
    } else {
      return Promise.reject(res.message)
    }
  }
}

const mutations = {
  setTrade(state, payload) {
    state.tradeInfo = payload
  }
}

const getters = {
  totalAmount: state => state.tradeInfo.totalAmount,
  userAddressList: state => state.tradeInfo.userAddressList,
  tradeNo: state => state.tradeInfo.tradeNo,
  totalNum: state => state.tradeInfo.totalNum,
  originalTotalAmount:state => state.tradeInfo.originalTotalAmount,
  couponInfoList: state => state.tradeInfo.couponInfoList,
  detailArrayList: state => state.tradeInfo.detailArrayList,
  activityReduceAmount:state => state.tradeInfo.activityReduceAmount,
  orderDetailVoList: state => state.tradeInfo.orderDetailVoList
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
}