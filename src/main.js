import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false // 关闭生产提示

import router from './router' // 路由

import store from './store' // vuex

import '@/mockServer' // mock 服务器

// 注册全局组件
import TypeNav from './components/TypeNav'
import Confirm from './components/Confirm'
Vue.component('TypeNav', TypeNav)
Vue.component('Confirm', Confirm)

// 导入验证规则
import '@/utils/vee-validate'

// Object.keys(rules).forEach(rule => {
//   extend(rule, rules[rule]);
// });

new Vue({
  render: h => h(App),
  router,
  store,
  beforeCreate(){
    Vue.prototype.$bus = this // 全局时间总线
  }
  // mounted(){
  //   console.log(this)
  // }
}).$mount('#app')
