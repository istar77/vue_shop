import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// 路由懒加载
const Home = _=> import('@/pages/Home')
const Login = _=> import('@/pages/Login')
const Register = _=> import('@/pages/Register')
const Search = _=> import('@/pages/Search')
const Detail = _=> import('@/pages/Detail') 
const ShopCart = _=> import('@/pages/ShopCart') 
const AddCartSuccess = _=> import('@/pages/AddCartSuccess')
const Trade = _=> import('@/pages/Trade')
const Pay = _=> import('@/pages/Pay')
const Center = _=> import('@/pages/Center')
const PaySuccess = _=> import('@/pages/PaySuccess')

/**
 * 1. 编程式导航的错误问题（跳转到相同路径）
 * 原因：3.1.0 之后编程式路由导航是基于 Promise 的，连续跳转到同一路径，
 * 会触发错误，但是 VueRouter 并没有给处理。所以，需要我们手动处理。
 * 解决1：在使用 $router.push 时，添加导航成功、失败回调函数
 * 解决2：使用.catch 捕获错误
 * 解决3：重写 $router原型是的push 方法（加上成功、失败处理回调）（统一处理0）
 */

// 统一处理编程式导航的错误问题
// 重写 push 和 replace 方法
const originPush = VueRouter.prototype.push
const originReplace = VueRouter.prototype.replace

VueRouter.prototype.push = function(location, onResolved, onRejected){
  if(onResolved && onRejected) {
     originPush.call(this, location, onResolved, onRejected)
  } else {
    return originPush.call(this,location, () => {}, () => {})
  }
}
VueRouter.prototype.replace = function(location, onResolved, onRejected) {
  onResolved && onRejected 
  ? originReplace.call(this, location,onResolved, onRejected)
  : originReplace.call(this, location, _=>{}, _=>{})
}


const router = new VueRouter({
  mode: 'history',
  base: '/vue/shop/',
  scrollBehavior(to, from, savedPosition) {
    // if(savedPosition) return savedPosition
    // else return {x: 0, y: 0}
    return savedPosition || {x: 0, y: 0}
  },
  routes: [
    {
      path: '/',
      redirect: '/home',
    },
    {
      name: 'home',
      path: '/home',
      component: Home,
      meta: {
        title: '主页 - 尚品汇商城',
        showTypeNav: true,
      }
    },
    {
      name: 'login',
      path: '/login',
      component: Login,
      meta: {
        title: '登录 - 尚品汇商城'
      }
    },
    {
      name: 'register',
      path: '/register',
      component: Register,
      meta: {
        title: '注册 - 尚品汇商城'
      }
    },
    {
      name: 'search',
      path: '/search/:keyword?',
      component: Search,
      meta: {
        title: '搜索 - 尚品汇商城'
      }
    },
    {
      name: 'detail',
      path: '/detail/:skuId',
      component: Detail,
      meta: {
        title: '详情 - 尚品汇商城'
      }
    },
    {
      name: 'shopcart',
      path: '/shopcart',
      component: ShopCart,
      meta: {
        title: '购物车 - 尚品汇商城'
      }
    },
    {
      name:'addCartSuccess',
      path: '/addCartSuccess',
      component:AddCartSuccess,
      meta: {
        title: '加购成功 - 尚品汇商城'
      }
    },
    {
      name: 'trade',
      path: '/trade',
      component: Trade,
      meta: {
        title: '提交订单 - 尚品汇商城'
      }
    },
    {
      name: 'pay',
      path: '/pay',
      component: Pay,
      meta: {
        title: '支付 - 尚品汇商城'
      }
    },
    {
      name: 'center',
      path: '/center',
      component: Center,
      meta: {
        title: '个人中心 - 尚品汇商城'
      }
    },
    {
      name: 'paysuccess',
      path: '/paysuccess',
      component: PaySuccess,
      meta: {
        title: '加购成功 - 尚品汇商城'
      }
    },
    {
      path: '*',
      redirect: '/',
    }
  ]
})

// 路由白名单，未登录用户是可以访问的
const whiteList = ['login', 'register', 'shopcart', 'search', 'detail', 'addcart','addCartSuccess']
router.beforeEach((to, from, next) => {
  if(!whiteList.includes(to.name) && !localStorage.getItem('token')) {
    next({
      path:'/login',
      query: {
        redirect: to.fullPath
      }
    })
  } else {
    next()
  }
})


export default router